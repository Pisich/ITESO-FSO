/*
    Práctica 1 Fundamentos de Sistemas Operativos
    [Versión con Hilos] Cálculo de PI con regla del trapecio

    Equipo 8
    is727366 - Carlos Eduardo Rodríguez Castro
*/

#include "librerias.h"


// Total de cores disponibles en el sistema
#define NTHREADS sysconf(_SC_NPROCESSORS_ONLN)

#define FACTOR_PI 4

int ITERS;             // Contenedor de cantidad de iteraciones
double factor;         // Variable para establecer cantidad de rectángulos
double g_sum = 0.0;    // Acumulador para resultado
pthread_mutex_t mutex; // Auxiliar para control de threads

// Función matemática para cuarto de la circunferencia
double f(double y)
{
  return sqrt(1 - pow(y, 2));
}

// Manejo de threads
void *tfunc(void *args)
{
  int tnum = *((int *)args);                                // ID del thread
  int inicio = (tnum == 0) ? 1 : tnum * (ITERS / NTHREADS); // Inicio de sección del cálculo (inclusivo)
  int fin = inicio + (ITERS / NTHREADS);                    // Fin de sección del cálculo (excluyente)
  double local = 0.0;                                       // Suma local (relacionado a segmentos de memoria compartida)


  // Aumentar acumulador con el área de los rectángulos
  for (int n = inicio; n < fin; n++)
    local += factor * f(((2 * n + 1.0) / ITERS) / 2);

  // Solicitar acceso a memoria para actualizar el resultado actual y luego liberarla
  pthread_mutex_lock(&mutex);
  g_sum += local;
  pthread_mutex_unlock(&mutex);
}

int main()
{
  // Variables relacionadas a threads
  pthread_t tid[NTHREADS]; // Arreglo de threads
  int targs[NTHREADS];     // Identificadores de threads

  int i;             // Auxiliar para iterador

  /* Segmento de variables para temporizador */
  long long start_ts;
  long long stop_ts;
  long long elapsed_time;
  long lElapsedTime;
  struct timeval ts;

  int x[6] = {1, 10, 100, 1000, 10000, 100000};
   for(int j = 0; j <= 5; j++){
  ITERS = x[j];
  factor = 1.0 / ITERS; // Guardado de separación de rectángulos

  // Iniciar temporizador
  gettimeofday(&ts, NULL);
  start_ts = ts.tv_sec;

  // Inicia segmento de paralelización
  pthread_mutex_init(&mutex, NULL); // Inicialización de controlador de threads

  // Creación de threads
  for (i = 0; i < NTHREADS; i++)
  {
    targs[i] = i; // Guardado de identificador de thread
    pthread_create(&tid[i], NULL, tfunc, &targs[i]);
  }

  // Conjunción de threads
  for (i = 0; i < NTHREADS; i++)
    pthread_join(tid[i], NULL);

  pthread_mutex_destroy(&mutex); // Finalizar controlador de threads

  // Multiplicar por 4 para obtener el resultado esperado
  g_sum *= FACTOR_PI;

  // Detener temporizador
  gettimeofday(&ts, NULL);
  stop_ts = ts.tv_sec;

  elapsed_time = stop_ts - start_ts; // Obtener tiempo de ejecución
  printf("%d %.30lf\n", ITERS,g_sum);
  g_sum = 0;
}}
