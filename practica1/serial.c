/*
    Práctica 1 Fundamentos de Sistemas Operativos
    [Versión Serial] Cálculo de PI con regla del trapecio

    Equipo 8
    is727366 - Carlos Eduardo Rodríguez Castro
*/

#include "librerias.h"

#define FACTOR_PI 4

// Función matemática para cuarto de la circunferencia
double f(double y)
{
  return sqrt(1 - pow(y, 2));
}

int main()
{
  double acc = 0;    // Acumulador para resultado
  int ITERS;         // Cantidad de iteraciones

  /* Variables para temporizador */
  long long start_ts;
  long long stop_ts;
  long long elapsed_time;
  long lElapsedTime;
  struct timeval ts;

  int x[6] = {1, 10, 100, 1000, 10000, 100000};
  for(int j = 0; j <= 5; j++){
  ITERS = x[j];
  double factor = 1.0 / ITERS;

  // Temporizador inicia
  gettimeofday(&ts, NULL);
  start_ts = ts.tv_sec;

  for (int i = 0; i < ITERS; i++)
  {

    // Aumentar acumulador con el área de cada rectángulos
    acc += factor * f(((2 * i + 1.0) / ITERS) / 2);
  }

  // Multiplicar por 4 para obtener el resultado esperado
  acc *= FACTOR_PI;

  // Detener temporizador
  gettimeofday(&ts, NULL);
  stop_ts = ts.tv_sec;

  elapsed_time = stop_ts - start_ts; // Obtener tiempo de ejecución
  printf("%d %.30lf\n", ITERS, acc);
  acc = 0;
}}
